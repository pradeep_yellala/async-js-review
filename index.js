// const data = require("./funds.json");
const fs = require("fs");
const fetch = require("node-fetch");

// const top10MutualFunds = (data) => {
//   const top10Funds = data
//     .sort((a, b) => b.returns.year_5 - a.returns.year_5)
//     .slice(0, 10);
//   console.log(top10Funds);
// };

// top10MutualFunds(data);

// const fundCategory = (data) => {
//   mutualFunds = {};
//   data.forEach(({ fund_house, fund_category }) => {
//     if (mutualFunds[fund_house]) {
//       if (mutualFunds[fund_house][fund_category]) {
//         mutualFunds[fund_house][fund_category] += 1;
//       } else {
//         mutualFunds[fund_house][fund_category] = 1;
//       }
//     } else {
//       mutualFunds[fund_house] = {};
//       mutualFunds[fund_house][fund_category] = 1;
//     }
//   });
//   console.log(mutualFunds);
// };
// fundCategory(arr);
// const averageReturn = (data, callback) => {
//   mutualFunds = {};
//   data.forEach(({ fund_house, returns }) => {
//     if (mutualFunds[fund_house]) {
//       if (mutualFunds[fund_house]["amount"]) {
//         mutualFunds[fund_house]["amount"] += returns["year_1"];
//         mutualFunds[fund_house]["number"] += 1;
//       } else {
//         mutualFunds[fund_house]["amount"] = returns["year_1"];
//         mutualFunds[fund_house]["number"] = 1;
//       }
//     } else {
//       mutualFunds[fund_house] = {};
//       mutualFunds[fund_house]["amount"] = returns["year_1"];
//       mutualFunds[fund_house]["number"] = 1;
//     }
//   });
//   const average = {};
//   Object.keys(mutualFunds).forEach((house) => {
//     const avg = (
//       mutualFunds[house]["amount"] / mutualFunds[house]["number"]
//     ).toFixed(2);

//     average[house] = avg;
//   });
//   callback(average);
// };
// function writeFile(data) {
//   fs.writeFile("./average.json", JSON.stringify(data), (err) => {
//     if (err) {
//       console.log(err);
//     }
//   });
// }

// function readFile(callback) {
//   fs.readFile("./funds.json", "utf-8", (err, data) => {
//     if (err) {
//       console.log(err);
//     } else {
//       callback(data)
//     }
//   });
// }
// readFile(
// function writeFile(data) {
//   return new Promise((resolve, reject) => {
//     fs.writeFile("./output7.json", JSON.stringify(data), (err) => {
//       if (err) {
//         reject(err);
//       } else {
//         resolve();
//       }
//     });
//   });
// }

// async function output() {
//   const response = await fetch("https://api.kuvera.in/api/v3/funds.json");
//   const data = await response.json();
//   const waitingData = await waiting(data);
//   await writeFile(waitingData);
// }

// output();

/* promises */
//
function writeFile(data, path) {
  return new Promise((resolve, reject) => {
    console.log("waiting for the data...");
    setTimeout(() => {
      fs.writeFile(path, JSON.stringify(data), (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    }, 5 * 1000);
  });
}

const filePaths = [
  "./public/promises/FR496-GR.json",
  "./public/promises/TRSSG1-GR.json",
  "./public/promises/BSD1050-GR.json",
  "./public/promises/606-DR.json",
  "./public/promises/8010-DP.json",
  "./public/promises/PCFFZM-DP.json",
];
const dataFile1 = fetch("https://api.kuvera.in/api/v3/funds/FR496-GR.json");
const dataFile2 = fetch("https://api.kuvera.in/api/v3/funds/TRSSG1-GR.json");
const dataFile3 = fetch("https://api.kuvera.in/api/v3/funds/BSD1050-GR.json");
const dataFile4 = fetch("https://api.kuvera.in/api/v3/funds/606-DR.json");
const dataFile5 = fetch("https://api.kuvera.in/api/v3/funds/8010-DP.json");
const dataFile6 = fetch("https://api.kuvera.in/api/v3/funds/PCFFZM-DP.json");

Promise.all([dataFile1, dataFile2, dataFile3, dataFile4, dataFile5, dataFile6])
  .then((files) => {
    files.forEach((file, index) => {
      process(file.json(), index);
    });
  })
  .catch((err) => {
    console.log(err);
  });
function process(response, index) {
  response.then((fileData) => {
    writeFile(fileData, filePaths[index]);
  });
}
